//
//  EmogiViewController.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 6/12/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

struct emCat {
    var name:String = ""
    var size:Int = 0
    
    init(name: String, size:Int) {
        self.name = name
        self.size = size
    }
}

class EmogiViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var emogiCategoryCollectionView: UICollectionView!
    @IBOutlet weak var emogiListCollectionView: UICollectionView!
    
    var categories = [emCat]()
    var emogiList = [UIImage]()
    var storage:StorageReference?
    var selectedCategory = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.storage = Storage.storage().reference()
        self.emogiCategoryCollectionView.delegate = self
        self.emogiListCollectionView.delegate = self
        self.setEmogiCategories()
        self.emogiListCollectionView.reloadData()
        self.emogiCategoryCollectionView.reloadData()
    }
    
    func setEmogiCategories(){
        self.categories.append(emCat(name: "whatsapp", size: 30))
        self.categories.append(emCat(name: "square", size: 33))
        self.categories.append(emCat(name: "poop", size: 9))
        self.categories.append(emCat(name: "animals", size: 9))
        self.categories.append(emCat(name: "dogs", size: 9))
        self.categories.append(emCat(name: "humanAndDog", size: 20))
        self.categories.append(emCat(name: "baby", size: 12))
        self.categories.append(emCat(name: "bear", size: 16))
        self.categories.append(emCat(name: "cats", size: 12))
        self.categories.append(emCat(name: "chicken", size: 9))
        self.categories.append(emCat(name: "dogs2", size: 12))
        self.categories.append(emCat(name: "dogs3", size: 9))
        self.categories.append(emCat(name: "frog", size: 14))
        self.categories.append(emCat(name: "heart", size: 8))
        self.categories.append(emCat(name: "panda2", size: 12))
        self.categories.append(emCat(name: "raccoon2", size: 9))
        self.categories.append(emCat(name: "human-face", size: 30))
        self.categories.append(emCat(name: "human-figure", size: 8))
        self.categories.append(emCat(name: "outlines", size: 96))
        self.categories.append(emCat(name: "panda", size: 12))
        self.categories.append(emCat(name: "penguin", size: 9))
        self.categories.append(emCat(name: "pigs", size: 9))
        self.categories.append(emCat(name: "pumpkins", size: 25))
        self.categories.append(emCat(name: "skulls", size: 25))
        self.categories.append(emCat(name: "round-emogi", size: 59))
        self.categories.append(emCat(name: "round-face", size: 15))
        self.categories.append(emCat(name: "raccoon", size: 9))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == emogiCategoryCollectionView
        {
            let vc = AppDelegate.secondview
            self.selectedCategory = indexPath.row
            self.emogiList.removeAll()
            self.emogiListCollectionView.reloadData()
            vc?.lockColorBtn()
            if categories[indexPath.row].name == "outlines"
            {
                vc?.unlockColorBtn()
            }
            
        }
        else if collectionView == emogiListCollectionView
        {
            // dismiss window
            let vc = AppDelegate.secondview
            let emogiCell:EmogiListCollectionViewCell = collectionView.cellForItem(at: indexPath) as! EmogiListCollectionViewCell
            vc?.topImage.image = emogiCell.emogiImage.image
            vc?.toogleEmogiList()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.emogiCategoryCollectionView
        {
            return self.categories.count
        }
        else if collectionView == self.emogiListCollectionView
        {
            return self.categories[self.selectedCategory].size
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == emogiListCollectionView
        {
            let emogiCell:EmogiListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emogiListCell", for: indexPath) as! EmogiListCollectionViewCell
            
            emogiCell.activityIndicator.hidesWhenStopped = true
            emogiCell.activityIndicator.startAnimating()
            self.storage?.child(categories[selectedCategory].name).child("\(indexPath.row + 1).png").getData(maxSize: 3*1000*1000) { (data, error) in
                if error != nil
                {
                    print(error ?? "Error downloading emogi")
                    return
                }
                self.emogiList.append(UIImage(data: data!)!)
                emogiCell.emogiImage.image = UIImage(data: data!)
                emogiCell.activityIndicator.stopAnimating()
            }
            return emogiCell
        }
        else if collectionView == emogiCategoryCollectionView
        {
            let emogiCell:EmogiCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emogiCategoryCell", for: indexPath) as! EmogiCollectionViewCell
            emogiCell.emogiImage.image = UIImage(named: self.categories[indexPath.row].name)
            emogiCell.activityIndicator.hidesWhenStopped = true
            emogiCell.activityIndicator.stopAnimating()
            return emogiCell
        }
        return UICollectionViewCell()
    }
}
