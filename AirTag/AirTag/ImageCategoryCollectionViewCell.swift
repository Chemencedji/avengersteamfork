//
//  ImageCategoryCollectionViewCell.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/9/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class ImageCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}
