//
//  EmogiCollectionViewCell.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 6/12/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class EmogiCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var emogiImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
