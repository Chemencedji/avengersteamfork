//
//  SearchViewController.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/12/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var images = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        searchBar.delegate = self
        self.hideKeyboardWhenTappedAround()
        //navigation bar bottom line
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.searchBar.layer.borderWidth = 1
        self.searchBar.layer.borderColor = self.searchBar.tintColor.cgColor
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showImage" {
            let vc = segue.destination as! ShowImageController
            let cell2 = sender as! SearchCollectionViewCell
            //print(vc.imageFromSegue)
            vc.image = cell2.image.image
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var bottomReached = false
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) && !bottomReached
        {
            //reach bottom
            print("bottom")
            bottomReached = true
            self.searchImages(nextPage: true)
        }
        
        if (scrollView.contentOffset.y < 0){
            //reach top
            print("top")
        }
        
        if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height))
        {
            bottomReached = false
            //not top and not bottom
            print("not bottom & not top")
        }
    }
    
    var nextIndex:Int!
    func searchImages(nextPage:Bool = false)
    {
        if let query = String(self.searchBar.text!)
        {
            print("\n\nQUERY=\n", query)
            var page = 1
            if nextPage
            {
                page = self.nextIndex
            }
            let info = URL(string: "https://www.googleapis.com/customsearch/v1?q=\(query))&start=\(page)&key=AIzaSyAnc2KQk4FWC3LiQ0xJCfBWWYP4UpjnREQ&cx=005545195455194668903:hyu4cgoznq8&searchType=image".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            
            var url = URLRequest(url: info!)
            url.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.alert(title: "Error", message: "Error in downloading the image")
                }
                else
                {
                    DispatchQueue.main.sync {
                        do {
                            let dic = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]

                            if dic["error"] != nil
                            {
                                let err = dic["error"] as! [String: Any]
                                let Errors = err["errors"] as! [NSDictionary]
                                self.alert(title: "Api Error", message: Errors[0].value(forKey: "message") as! String)
                                return
                            }
                            if dic["queries"] != nil
                            {
                                let que = dic["queries"] as! [String: Any]
                                if let next = que["nextPage"] as? [NSDictionary]
                                {
                                    self.nextIndex = Int(String(describing: next[0].value(forKey: "startIndex")!))
                                }
                            }
                            if dic["items"] != nil
                            {
                                let imagesDic = dic["items"] as! [NSDictionary]
                                for img in imagesDic
                                {
                                    print(img)
                                    self.images.append(String(describing: img.value(forKey: "link")!))
                                }
                                print("\n\n\n", self.images, "\n\n\n")
                                self.collectionView.reloadData()
                            }
                            else
                            {
                                print("No more photos")
                            }
                            
                        }
                        catch let err
                        {
                            print(err)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBegan")
        self.images.removeAll()
        self.searchImages()
    }
    
    func alert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okBTN = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(okBTN)
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageFromSearchCell", for: indexPath) as! SearchCollectionViewCell
        
        let url = URL(string: images[indexPath.row])
        
        let download = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            if error != nil
            {
                self.alert(title: "Ooups, an error has ocured", message: String(describing: error))
            }
            else
            {
                DispatchQueue.main.async {
                    cell.activityIndicator.hidesWhenStopped = true
                    cell.activityIndicator.stopAnimating()
                    cell.image.image = UIImage(data: data!)
                }
            }
        }
        download.resume()
        cell.activityIndicator.startAnimating()
        cell.layer.borderColor = (UIColor.black.cgColor)
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 5
        return cell
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
