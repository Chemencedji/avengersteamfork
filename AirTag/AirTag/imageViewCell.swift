//
//  imageViewCell.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 5/10/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class imageViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var fontField: UILabel!
    
}
